<?php

use App\Http\Controllers\Backend\Project;

Route::post('import', [Project::class, 'import'])->name('project.import');

