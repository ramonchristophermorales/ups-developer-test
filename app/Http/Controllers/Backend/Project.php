<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Http\Requests\Backend\ProjectImportRequest;

use App\Models\Project as ProjectModel;

class Project extends Controller
{

    /**
     * save the imported file data
     *
     * @param  App\Http\Requests\Backend\ProjectImportRequest  $request
     * @return \Redirect
     */
    public function import(ProjectImportRequest $request)
    {
        if (!$request->hasFile('importFile') || !$request->file('importFile')->isValid())
            return redirect()->back()->withErrors('Error: Import file not found. Please report this to the admin for more info.');
    
        try {
            $xmlString = file_get_contents($request->file('importFile'));
            $xmlObj = simplexml_load_string($xmlString);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Error: Something went wrong while importing xml file. Please report this to the admin for more info.');
        }
        

        $xmlObj = json_decode(json_encode($xmlObj));
            
        if (!isset($xmlObj->row))
            return redirect()->back()->withErrors('Error: Import file does not contain the right xml format.');
            
        $xmlArr = $xmlObj->row;

        if (count($xmlArr) < 1)
            return redirect()->back()->withErrors('Error: Import file does not contain project data.');
        
        $data = [];
            
        foreach ($xmlArr as $v) :
            if (!isset($v->name) || !isset($v->description))
                return redirect()->back()->withErrors('Error: Import file does not contain project data.');
                
            $data[] = [
                'name' => $v->name,
                'description' => $v->description,
            ];
        endforeach;
        
        try {
            ProjectModel::insert($data);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors('Error: Something went wrong while saving data from imported xml file. Please report this to the admin for more info.');
        }
        
        return redirect()->back()->with('flash_success', 'Success: File Imported');
    }

    /**
     * Display projects on json
     *
     * @return \Illuminate\Http\Response
     */
    public function projects()
    {
        $projects = ProjectModel::all();
        
        return response()->json($projects);
    }

    /**
     * Display a project on json
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function project($id=0)
    {
        $project = ProjectModel::find($id);
        
        return response()->json($project);
    }

}
