## Installation instructions
1. Clone repository from:	[https://bitbucket.org/ramonchristophermorales/ups-developer-test/src/master/](https://bitbucket.org/ramonchristophermorales/ups-developer-test/src/master/)
2. Composer Install
3. Import mysql database project.sql to project db
4. Rename .env.example to .env and configure database connection according to local
5. Use login credentials:
			User: admin@admin.com
			Pass: secret

Test file: project.xml