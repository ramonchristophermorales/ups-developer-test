<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }}
            <a href="#">
                UPS - Developer Test
            </a>
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

</footer>
